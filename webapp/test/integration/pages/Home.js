sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"opu5_testing_app/test/integration/pages/Common",
	"sap/ui/test/matchers/PropertyStrictEquals"
], function(Opa5, Press, Common, PropertyStrictEquals) {
	"use strict";

	var sViewName = "Home";

	Opa5.createPageObjects({
		onTheHomePage: {
			baseClass: Common,

			actions: {
				shouldSeeButtonText: function() {
										return this.waitFor({
						//id: "navigation",
						viewName: sViewName,
						controlType : "sap.m.Button",
						matchers: new PropertyStrictEquals({
							text: "Press"
						}),
							success: function () {
								Opa5.assert.ok(true, "Button text is correct");
							},
						errorMessage: "Wrong button text"
					});
				},
				
				iPressThePressButton: function() {
					return this.waitFor({
						//id: "navigation",
						viewName: sViewName,
						controlType : "sap.m.Button",
						//matchers: new PropertyStrictEquals({
						//	text: "Press"
						//}),
						actions: new Press(),
						errorMessage: "Did not find the button on home page"
					});
				}
			}
		}
	});
});