sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"opu5_testing_app/test/integration/pages/Common"
], function(Opa5, Press, Common) {
	"use strict";

	var sViewName = "DataList";

	Opa5.createPageObjects({
		onTheDataListPage: {
			baseClass: Common,

			assertions: {
				iShouldSeeTheTable  : function() {
					return this.waitFor({
					id: "idMyRecords",
					viewName: sViewName,
					success: function () {
								Opa5.assert.ok(true, "The table is visible");
							},
							errorMessage: "Was not able to see the table."
						});
				}
			}
		}
	});
});