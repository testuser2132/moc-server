sap.ui.define([
	"sap/ui/test/opaQunit"
], function(opaTest) {
	"use strict";

	QUnit.module("Desktop navigation");

	opaTest("Should navigate on press", function(Given, When, Then) {
		// Arrangements
		Given.iStartTheApp({delay: 2000});
		
		// Actions
		When.onTheHomePage.shouldSeeButtonText();
		When.onTheHomePage.iPressThePressButton();

		// Assertions
		Then.onTheDataListPage.iShouldSeeTheTable();
	});

});