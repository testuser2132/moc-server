jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"opu5_testing_app/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"opu5_testing_app/test/integration/pages/DataList",
	"opu5_testing_app/test/integration/pages/Home"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "opu5_testing_app.view."
	});

	sap.ui.require([
		"opu5_testing_app/test/integration/NavigationJourney"
	], function () {
		QUnit.start();
	});
});