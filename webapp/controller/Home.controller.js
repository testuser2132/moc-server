sap.ui.define([
	"opu5_testing_app/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("opu5_testing_app.controller.Home", {
		onInit: function () {
			
		},
		
		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},
		
		onNavToNavigate: function() {
			this.getRouter().navTo("dataList");
		}
	});
});